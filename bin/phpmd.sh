#!/bin/sh

set -eu

vendor/bin/phpmd tests/ text ruleset
vendor/bin/phpmd src/ text ruleset
