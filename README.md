# uploader-php-client

The reference implementation of a PHP client for uploader

[![build status](https://gitlab.com/twinscom/uploader-php-client/badges/master/build.svg)](https://gitlab.com/twinscom/uploader-php-client/builds)

## API

```php
<?php

$uploader = new twinscom\Uploader\Client([
    'projectInstanceId' => 'project-instance-id',
    'baseUri' => 'http://localhost/',
    'apiKey' => 'api-key',
    'acceptLanguage' => 'de', // optional
]);

$uploader->uploadFile([
    'name' => 'filename.txt',
    'file' => '/tmp/file',
    'onSuccess' => function (array $fileModel) {
        print_r($fileModel);
    },
    'onError' => function ($message) {
        echo $message;
    },
]);

$uploader->getFile([
    'id' => 'file-id',
    'onSuccess' => function (array $fileModel) {
        print_r($fileModel);
    },
    'onError' => function ($message) {
        echo $message;
    },
]);

$uploader->getFiles([
    'ids' => [
        'file-id',
        'other-file-id',
    ],
    'onSuccess' => function (array $fileModels) {
        print_r($fileModels);
    },
    'onError' => function ($message) {
        echo $message;
    },
]);

$uploader->deleteFile([
    'id' => 'file-id',
    'onSuccess' => function () {
        echo 'Deleted!';
    },
    'onError' => function ($message) {
        echo $message;
    },
]);

```

## License

[MIT](LICENSE)
