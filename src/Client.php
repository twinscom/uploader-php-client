<?php

namespace twinscom\Uploader;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Psr\Http\Message\ResponseInterface;

/**
 * Reference client for Uploader service.
 */
class Client
{
    /**
     * @var string the version of the API
     */
    private $apiVersion = '1.0.0';

    /**
     * @var \GuzzleHttp\Client an instance of the HTTP client
     */
    private $httpClient;

    /**
     * @var string project instance ID
     */
    private $projectInstanceId;

    /**
     * @var string the API key
     */
    private $apiKey;

    /**
     * @var array default headers
     */
    private $defaultHeaders;

    /**
     * Client constructor.
     *
     * @param array $options Options.
     *
     * An associative array with the following keys:
     * - projectInstanceId: string, project instance ID,
     * - baseUri: string, Uploader service's base URI,
     * - apiKey: string, Uploader service's API key,
     * - acceptLanguage: string, optional, the "Accept-Language" header.
     */
    public function __construct(array $options)
    {
        $this->projectInstanceId = $options['projectInstanceId'];
        $this->apiKey = $options['apiKey'];

        $this->defaultHeaders = [
            'Accept-Version' => $this->apiVersion,
        ];

        if (\array_key_exists('acceptLanguage', $options)) {
            $this->defaultHeaders['Accept-Language'] =
                $options['Accept-Language'];
        }

        $this->httpClient = new \GuzzleHttp\Client([
            /* v5.3.0 options */
            'base_url' => $options['baseUri'],
            'defaults' => [
                'headers' => $this->defaultHeaders,
                'query' => [
                    'project_instance_id' => $this->projectInstanceId,
                    'api_key' => $this->apiKey,
                ],
            ],

            /* v6.0.0 options */
            'base_uri' => $options['baseUri'],
            'headers' => $this->defaultHeaders,
            'query' => [
                'project_instance_id' => $this->projectInstanceId,
                'api_key' => $this->apiKey,
            ],
        ]);
    }

    /**
     * Uploads a file.
     *
     * @param array $options Options.
     *
     * An associative array with the following keys:
     * - name: string, file name,
     * - file: string, path to the file,
     * - onSuccess: callable, optional, receives a file model as an
     * associative array: function onSuccess(array $result) {},
     * - onError: callable, optional, receives an error message as a
     * string: function onError($message) {}.
     */
    public function uploadFile(array $options)
    {
        $getResponse = function () use ($options) {
            $headers = \array_merge($this->defaultHeaders, [
                'X-File' => $options['file'],
            ]);

            return $this->httpClient->post('files', [
                'query' => [
                    'name' => $options['name'],
                    'project_instance_id' => $this->projectInstanceId,
                    'api_key' => $this->apiKey,
                ],
                'headers' => $headers,
            ]);
        };

        $this->handleResponse($getResponse, $options);
    }

    /**
     * Gets a file model.
     *
     * @param array $options Options.
     *
     * An associative array with the following keys:
     * - id: string, file ID,
     * - onSuccess: callable, optional, receives a file model as an
     * associative array: function onSuccess(array $result) {},
     * - onError: callable, optional, receives an error message as a
     * string: function onError($message) {}.
     */
    public function getFile(array $options)
    {
        $getResponse = function () use ($options) {
            return $this->httpClient->get("files/{$options['id']}");
        };

        $this->handleResponse($getResponse, $options);
    }

    /**
     * Gets a list of file models.
     *
     * @param array $options Options.
     *
     * An associative array with the following keys:
     * - ids: array, file IDs,
     * - onSuccess: callable, optional, receives an array of file models:
     * function onSuccess(array $result) {},
     * - onError: callable, optional, receives an error message as a
     * string: function onError($message) {}.
     */
    public function getFiles(array $options)
    {
        $getResponse = function () use ($options) {
            return $this->httpClient->get('files', [
                'query' => [
                    'file_ids' => implode(',', $options['ids']),
                    'project_instance_id' => $this->projectInstanceId,
                    'api_key' => $this->apiKey,
                ],
            ]);
        };

        $this->handleResponse($getResponse, $options);
    }

    /**
     * Deletes a file and its derivatives.
     *
     * @param array $options Options.
     *
     * An associative array with the following keys:
     * - id: string, file ID,
     * - onSuccess: callable, optional, doesn't receive anything:
     * function onSuccess() {},
     * - onError: callable, optional, receives an error message as a
     * string: function onError($message) {}.
     */
    public function deleteFile(array $options)
    {
        $getResponse = function () use ($options) {
            return $this->httpClient->delete("files/{$options['id']}");
        };

        $this->handleResponse($getResponse, $options);
    }

    /**
     * Gets the message from an erroneous response and calls the "onError"
     * callback if it exists.
     *
     * @param RequestException $error
     * @param array            $options
     */
    private function handleErroneousResponse(
        RequestException $error,
        array $options
    ) {
        if (!\array_key_exists('onError', $options)) {
            return;
        }

        $body = $error->getResponse()->getBody();
        $uploaderError = \json_decode($body, true);
        $options['onError']($uploaderError['message']);
    }

    /**
     * Gets the message from a RequestException and calls the "onError"
     * callback if it exists.
     *
     * @param RequestException $error
     * @param array            $options
     */
    private function handleRequestException(
        RequestException $error,
        array $options
    ) {
        if (!\array_key_exists('onError', $options)) {
            return;
        }

        $message = $error->getMessage();
        $options['onError']($message);
    }

    /**
     * @param callable $getResponse callable that returns a ResponseInterface
     * @param array    $options
     */
    private function handleResponse(callable $getResponse, array $options)
    {
        try {
            /** @var ResponseInterface $response */
            $response = $getResponse();
        } catch (ClientException $e) {
            $this->handleErroneousResponse($e, $options);

            return;
        } catch (ServerException $e) {
            $this->handleErroneousResponse($e, $options);

            return;
        } catch (RequestException $e) {
            $this->handleRequestException($e, $options);

            return;
        }

        if (!\array_key_exists('onSuccess', $options)) {
            return;
        }

        $body = $response->getBody();
        $result = \json_decode($body, true);
        $options['onSuccess']($result);
    }
}
